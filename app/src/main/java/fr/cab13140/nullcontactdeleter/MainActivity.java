package fr.cab13140.nullcontactdeleter;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.animation.*;
import android.view.*;
import android.content.DialogInterface;

public class MainActivity extends AppCompatActivity {
    Handler mHandler;
    private DeleteThread thread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        final MainActivity t = this;
        setContentView(R.layout.mainbg);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS,Manifest.permission.READ_CONTACTS}, 0);
        }else{
            System.out.println("Permissions - OK");
        }

        final ImageView image = findViewById(R.id.infoImage);
        final ProgressBar circle = findViewById(R.id.mainCircle);
        Button b = findViewById(R.id.startButton);
        Button ib = findViewById(R.id.infoButton);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            b.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_corners_blue));
        } else {
            //noinspection deprecation
            b.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_corners_blue));
        }
        b.setTextColor(Color.parseColor("#2196f3"));

        ib.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View p1) {
                AlertDialog d = new AlertDialog.Builder(MainActivity.this)
                        .setView(R.layout.about_dialog)
                        .create();
                d.show();

                ImageView playstore = d.findViewById(R.id.playstore);
                ImageView bitbucket = d.findViewById(R.id.bitbucket);
                ImageView donate = d.findViewById(R.id.donate);

                assert playstore != null;
                playstore.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View p1) {
                        Toast t = Toast.makeText(MainActivity.this, getString(R.string.soon), Toast.LENGTH_SHORT);
                        t.show();
                    }
                });

                assert bitbucket != null;
                bitbucket.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View p1) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse("https://bitbucket.org/_cab13_/"));
                        startActivity(i);
                    }
                });

                assert donate != null;
                donate.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View p1) {
						AlertDialog d = new AlertDialog.Builder(MainActivity.this)
							.setTitle("Donate")
							.setMessage("This app is free, but I accept thanks and donations. Note : Google takes a lot out of IAP, so better use Paypal.")
							.setPositiveButton("Paypal", new DialogInterface.OnClickListener(){

								@Override
								public void onClick(DialogInterface p1, int p2)
								{
									Intent i = new Intent(Intent.ACTION_VIEW);
									i.setData(Uri.parse("https://www.paypal.me/defvs"));
									startActivity(i);
								}

								
							})
							.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener(){

								@Override
								public void onClick(DialogInterface p1, int p2)
								{
									p1.dismiss();
								}
								
								
							})
							.setNeutralButton("Play IAP", new DialogInterface.OnClickListener(){

								@Override
								public void onClick(DialogInterface p1, int p2)
								{
									// TODO : IAP
								}
								
								
							})
							.create();
						d.show();
                    }
                });
            }


        });

        image.setImageResource(R.drawable.ready_icon);

        TextView text = findViewById(R.id.finalInfoText);
        text.setText(getString(R.string.ready));
        circle.setVisibility(View.INVISIBLE);
        image.setVisibility(View.VISIBLE);
        findViewById(R.id.startButton).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View p1) {
                circle.setVisibility(View.VISIBLE);
                image.setVisibility(View.INVISIBLE);
                TextView tv = findViewById(R.id.finalInfoText);
                if (tv != null) {
                    tv.setText(getString(R.string.loading));
                    ContentResolver cr = getContentResolver();
                    mHandler = new Handler();
                    thread = new DeleteThread(mHandler, cr, tv, t);
                    thread.start();
                }
            }
        });

    }

    public void onThreadResult(int code) {
        ImageView image = findViewById(R.id.infoImage);
        ProgressBar circle = findViewById(R.id.mainCircle);
        FrameLayout layout = findViewById(R.id.mainbgLayout);
        Button b = findViewById(R.id.startButton);
        Button ib = findViewById(R.id.infoButton);
        final TextView text = findViewById(R.id.finalInfoText);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            b.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_corners));
        } else {
            //noinspection deprecation
            b.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_corners));
        }
        b.setTextColor(Color.WHITE);

        ib.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ib.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_corners));
        } else {
            //noinspection deprecation
            ib.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rounded_corners));
        }
        ib.setTextColor(Color.WHITE);
		int code2;
		
		if (code == 3){
			text.setText(text.getText() + "\n" + getString(R.string.good_text));
			image.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.icon_question));
			code = 1;
			code2 = 3;
		}else code2 = 1;

        if (code == 1) {
            b.setText(getString(R.string.close));
            b.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View p1) {
                    finish();
                }


            });
            if (code2 == 1) text.setText(text.getText() + "\n" + getString(R.string.success_text));
            circle.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
			
			ColorDrawable[] colors = {new ColorDrawable(Color.WHITE), new ColorDrawable(Color.parseColor("#4caf50"))};

            if (code2== 3){colors[1] = new ColorDrawable(Color.parseColor("#ffab00"));}


            FrameLayout revealLayout = findViewById(R.id.secondLayout);
            int cx = revealLayout.getWidth() / 2;
            int cy = revealLayout.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(revealLayout, cx, cy, 0, finalRadius);
                revealLayout.setVisibility(View.VISIBLE);
                revealLayout.setBackgroundDrawable(colors[1]);
                anim.start();
            }else{
                TransitionDrawable trans = new TransitionDrawable(colors);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout.setBackground(trans);
                } else {
                    //noinspection deprecation
                    layout.setBackgroundDrawable(trans);
                }
                trans.startTransition(1000);
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityManager.TaskDescription tDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher), code == 1 ? Color.parseColor("#FF8F00") : Color.parseColor("#388E3C"));
                setTaskDescription(tDesc);
            }
            ValueAnimator textTrans = ValueAnimator.ofInt(0, 255);
            textTrans.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
                @Override
                public void onAnimationUpdate(ValueAnimator p1) {
                    int value;
                    value = ((Integer) p1.getAnimatedValue());
                    text.setTextColor(Color.rgb(value, value, value));
                }
            });
            textTrans.setDuration(500);
            textTrans.start();
            if(code2 == 1) image.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.success_icon));
            image.setColorFilter(Color.WHITE);

        } else if (code == 2) {
            b.setText(getString(R.string.retry));
            b.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View p1) {
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                    finish();
                }


            });
            text.setText(R.string.error_text);
            circle.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            ColorDrawable[] colors = {new ColorDrawable(Color.WHITE), new ColorDrawable(Color.parseColor("#f44336"))};
            TransitionDrawable trans = new TransitionDrawable(colors);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                layout.setBackground(trans);
            } else {
                //noinspection deprecation
                layout.setBackgroundDrawable(trans);
            }
            trans.startTransition(1000);

            ValueAnimator textTrans = ValueAnimator.ofInt(0, 255);

            textTrans.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
                @Override
                public void onAnimationUpdate(ValueAnimator p1) {
                    int value = ((Integer) p1.getAnimatedValue());
                    text.setTextColor(Color.rgb(value, value, value));
                }
            });
            textTrans.setDuration(500);
            textTrans.start();
            image.setImageResource(R.drawable.error_icon);
            image.setColorFilter(Color.WHITE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case 0: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, R.string.perm_granted, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, R.string.perm_denied, Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            }
        }
    }
}
